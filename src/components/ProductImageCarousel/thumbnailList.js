import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from '@magento/peregrine';

import classify from '@magento/venia-library/esm/classify';
import Thumbnail from '@magento/venia-library/esm/components/ProductImageCarousel/thumbnail';
import defaultClasses from '@magento/venia-library/src/components/ProductImageCarousel/thumbnailList.css';

class ThumbnailList extends Component {
    static propTypes = {
        activeItemIndex: PropTypes.number,
        classes: PropTypes.shape({
            root: PropTypes.string
        }),
        items: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                position: PropTypes.number,
                disabled: PropTypes.bool,
                file: PropTypes.string.isRequired
            })
        ).isRequired,
        updateActiveItemIndex: PropTypes.func.isRequired
    };

    updateActiveItemHandler = newActiveItemIndex => {
        this.props.updateActiveItemIndex(newActiveItemIndex);
    };

    render() {
        const { activeItemIndex, items, classes } = this.props;

        return (
            <div>
                <div>ABSO List</div>
                <List
                    items={items}
                    renderItem={props => (
                        <Thumbnail
                            {...props}
                            isActive={activeItemIndex === props.itemIndex}
                            onClickHandler={this.updateActiveItemHandler}
                        />
                    )}
                    getItemKey={i => i.file}
                    classes={classes}
                />
            </div>
        );
    }
}

export default classify(defaultClasses)(ThumbnailList);
