import React from 'react';
import { Switch, Route } from "@magento/venia-drivers";
import { Page } from '@magento/peregrine';
import ErrorView from "@magento/venia-library/esm/components/ErrorView";
import CreateAccountPage from "@magento/venia-library/esm/components/CreateAccountPage";
import Search from "@magento/venia-library/esm/RootComponents/Search";
import AccountPage from "../../../src/RootComponents/Account";

const renderRoutingError = props => React.createElement(ErrorView, props);

const renderRoutes = () => React.createElement(Switch, null, React.createElement(Route, {
    exact: true,
    path: "/search.html",
    component: Search
}), React.createElement(Route, {
    exact: false,
    path: "/create-account",
    component: CreateAccountPage
}), React.createElement(Route, {
    exact: true,
    path: "/account.html",
    component: AccountPage
}), React.createElement(Route, {
    render: () => React.createElement(Page, null, renderRoutingError)
}));

export default renderRoutes;
//# sourceMappingURL=renderRoutes.js.map