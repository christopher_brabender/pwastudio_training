import { connect } from '@magento/venia-library/esm/drivers';

import appActions, { closeDrawer } from '@magento/venia-library/esm/actions/app';
import App from './app';

const mapStateToProps = ({ app, unhandledErrors }) => ({
    app,
    unhandledErrors
});
const { markErrorHandled } = appActions;
const mapDispatchToProps = { closeDrawer, markErrorHandled };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);