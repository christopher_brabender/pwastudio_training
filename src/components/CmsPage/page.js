import React, { Component } from 'react';
import { shape, string } from 'prop-types';

import classify from '@magento/venia-library/esm/classify';
import defaultClasses from './page.css';

class Page extends Component {
    static propTypes = {
        classes: shape({
            root: string
        }),
        content: string.isRequired
    };

    render() {
        const { classes, content: __html } = this.props;

        return (
            <div
                className={classes.root}
                dangerouslySetInnerHTML={{ __html }}
            />
        );
    }
}

export default classify(defaultClasses)(Page);
