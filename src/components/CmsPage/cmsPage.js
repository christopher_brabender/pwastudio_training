import React, { Component } from 'react';
import { func, shape, string, integer } from 'prop-types';

import { Query } from '@magento/venia-library/esm/drivers';
import classify from '@magento/venia-library/esm/classify';
import Page from './page';
import defaultClasses from './cmsPage.css';
import getCmsPage from '../../queries/getCmsPage.graphql';

class cmsPage extends Component {
    static propTypes = {
        children: func,
        classes: shape({
            block: string,
            content: string,
            root: string
        }),
        id: integer
    };

    renderPage = ({ data, error, loading }) => {
        console.log(data);
        const { children, classes } = this.props;

        if (error) {
            return <div>Data Fetch Error</div>;
        }

        if (loading) {
            return <div>Fetching Data</div>;
        }

        const item = data.cmsPage;

        const PageChild = typeof children === 'function' ? children : Page;
        const page = (
            <PageChild
                key={item.url_key}
                className={classes.block}
                content={item.content}
            />
        );

        return <div className={classes.content}>{page}</div>;
    };

    render() {
        const { props, renderPage } = this;
        const { classes, id } = props;


        return (
            <div className={classes.root}>
                <Query query={getCmsPage} variables={ { id: 10, onServer: true } }>
                    {renderPage}
                </Query>
            </div>
        );
    }
}

export default classify(defaultClasses)(cmsPage);
