import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'src/drivers';
import { compose } from 'redux';
import classify from '@magento/venia-library/esm/classify';
import { loadingIndicator } from '@magento/venia-library/esm/components/LoadingIndicator';
import customerOrdersQuery from '../../../src/queries/getCustomerOrders.graphql';

import VeniaAdapter from '@magento/venia-library/esm/drivers/adapter';

import {Query} from "react-apollo";
import {ApolloClient} from "apollo-client";
import {BrowserPersistence} from "@magento/peregrine/esm/util";
import { setContext } from 'apollo-link-context';

export default class Account extends Component {

    render() {
        const storage = new BrowserPersistence();
        const signin_token = storage.getItem('signin_token');

        const authLink = setContext((_, { headers }) => {
            const token = signin_token;
            return {
                headers: {
                    ...headers,
                    authorization: `Bearer ${token}`
                }
            };
        });

        const apolloClient = new ApolloClient({
            link: authLink.concat(VeniaAdapter.apolloLink('/graphql')),
            cache: VeniaAdapter.apolloCache()
        });

        console.log("ACCOUNT COMPONENT");
        return (
            <div>
                <h1>Customer Account Dashboard</h1>
                <Query
                    client={apolloClient}
                    query={customerOrdersQuery}
                >
                    {({ loading, error, data }) => {
                        if (error) return <div>Data Fetch Error</div>;
                        if (loading) return loadingIndicator;

                        const orders = data.customerOrders.items;
                        console.log(orders);

                        return (
                            <div>Test</div>
                        );
                    }}
                </Query>
            </div>
        );
    }

}