const Buildpack = require('@magento/pwa-buildpack');

module.exports = async function setupBuildpackBuild(webpackCliEnv) {
    const config = await Buildpack.configureWebpack({
        context: __dirname,
        rootComponentPaths: [
            await Buildpack.resolveModuleDirectory(
                '@magento/venia-library',
                'esm/RootComponents',
            ),
            'src/RootComponents'
        ],
        webpackCliEnv
    });
    // Modify Webpack configuration object here if necessary.
    config.resolve.alias['@magento/venia-drivers'] = 'src/drivers';
    config.resolve.alias['./thumbnailList'] = 'src/components/ProductImageCarousel/thumbnailList.js';
    config.resolve.alias['./footer'] = 'src/components/Footer/footer.js';
    config.resolve.alias['../../queries/getNavigationMenu.graphql'] = 'src/queries/getNavigationMenu.graphql';
    config.resolve.alias['../../../queries/getNavigationMenu.graphql'] = 'src/queries/getNavigationMenu.graphql';

    return config;
};